const express = require('express');
const router = new express.Router();
const books = require('./books');

router.use('/book', books);

module.exports = router;