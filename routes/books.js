const express = require('express');
const router = new express.Router();
const check = require('../controllers/check-similar')
// Controllers 
const dataControl = require('../controllers/contollers');

router.post('/create',check, dataControl.create);
router.put('/update', dataControl.update);
router.get('/search', dataControl.search);
router.get('/:name', dataControl.searchByName);
router.delete('/delete', dataControl.delete);

module.exports = router;