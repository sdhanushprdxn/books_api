const express = require('express');
const bodyParser = require('body-parser');
const app = express();

const routes = require('./routes/routes')
require('./database connectivity/database-config')

// Middleware to read body sent as request
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Routes
app.use('/api',routes);

app.listen(3000, () => console.log('Server has started on port 3000...'));