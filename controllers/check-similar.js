const BookModel = require('../model/book-model');
const check = (req,res,next)=> {
  BookModel.findOne({name: req.body.name}).then( result => {
    if (result) {
      return res.send({status:"Already Exists", display:result});
    }
    else {
      return (next());
    }
  })
}
module.exports = check;