const BookModel = require('../model/book-model');

module.exports = {
  // Create controller
  create: (req, res) => {
    let book = new BookModel({
      name: req.body.name,
      author: req.body.author,
      email: req.body.email,
      price: req.body.price,
      type: req.body.type
    });

    book.save()
      .then(result => {
        res.json({ success: true, result: result });
      })
      .catch(err => {
        res.json({ success: false, result: `${err}` });
      });
  },
  // Update controller
  update: (req, res) => {
    BookModel.updateOne({_id: req.body._id}, req.body)
      .then(book => {
        if (book.n===0){
           return res.json({ success: false, result: "Book does not exist" });
        }
        else{  
          return res.json(book);
        }
      })
      .catch(err => {
        res.json({ success: false, result:`${err}`});
      });
  },
  // Search controller
  search: async (req, res) => {
    try{
      const books = await BookModel.find();
      if (books.length === 0){ 
        res.json({ success: false, result: "No results found" });
      }
      else{
        res.send({ books: books});
      }
    } catch(err){
      res.json({message:`${err}`});
    }
  },
  
  //search by name
  searchByName : (req,res) => {
    BookModel.find({name:req.params.name})
      .then( result => {
        console.log(result)
        if(result.length === 0) {
          return res.send({ success: false, result: "No results found" });
        }
        else {
          return res.send({book:result});
        }
      })
      .catch(err => res.json({success: false, result:`${err}` }));;
  },
  
  // Delete controller
  delete: (req, res) => {
    BookModel.deleteOne({_id: req.body._id})
    .then(result => {
        if (result.n === 0) {
          return res.json({ success: false, result: "No book was found with the ID" });
        }
        else{
          return res.json({ success: true, result: result });
        }
      })
      .catch(err => res.json({ success: false, result: `${err}` }));
  }
}